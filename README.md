# Coup (Web Game)
Projeto gerado com Angular 7.3.3 sobre o jogo de tabuleiro "[Coup](https://ludopedia.com.br/jogo/coup)". Simples implementação usando Angular com servidor em Kotlin, veja o [repositório](https://gitlab.uspdigital.usp.br/coup-dev/coup-server) do servidor.

# Desenvolvimento

## Development server

Para executar um servidor para desenvolvimento (com live reload) execute `ng serve`. O servidor estará em`http://localhost:4200/`.

## Gerar componentes

Para gerar componentes execute `ng generate component nome-do-componente`. Você também pode fazer `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Execute `ng build` para compilar o projeto. O resultado estará no diretório `dist/`.

## Executando testes (WIP):

Execute `ng test` para rodar os testes unitários usando [Karma](https://karma-runner.github.io).
