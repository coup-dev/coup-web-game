import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-top-bar',
  standalone: true,
  imports: [],
  templateUrl: './top-bar.component.html',
  styleUrl: './top-bar.component.scss'
})
export class TopBarComponent {
  loggedIn = false;
  constructor(private authService: AuthService) {
    this.authService.loggedIn.subscribe((value) => {
      this.loggedIn = value;
    });
  }


  get username() {
    return this.authService.username;
  }
}
