import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameRoomManagerComponent } from './game-room-manager.component';

describe('GameRoomManagerComponent', () => {
  let component: GameRoomManagerComponent;
  let fixture: ComponentFixture<GameRoomManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [GameRoomManagerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(GameRoomManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
