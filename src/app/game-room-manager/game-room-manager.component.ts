import { Component } from '@angular/core';
import { ServerService } from '../services/server.service';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NonNullAssert } from '@angular/compiler';

@Component({
  selector: 'app-game-room-manager',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './game-room-manager.component.html',
  styleUrl: './game-room-manager.component.scss'
})
export class GameRoomManagerComponent {
  tab: number = 0;

  constructor(private serverService: ServerService, private router: Router) {}

  createRoom() {
    const roomName: HTMLInputElement | null = document.querySelector('input[name="roomName"]');
    const sessionType: HTMLInputElement | null = document.querySelector('input[name="sessionType"]:checked');
    const maxPlayers: HTMLInputElement | null = document.querySelector('input[name="maxPlayers"]');
    const sessionTimeout: HTMLInputElement | null = document.querySelector('input[name="sessionTimeout"]');
    const room = {
      sessionName: roomName?.value || 'New Room',
      sessionType: sessionType?.value || 1,
      maxPlayers: maxPlayers?.value || 4,
      sessionTimeout: parseInt(sessionTimeout?.value || '60', 10)*1000
    }

    console.log('Creating room:', room);

    this.serverService.postToServer('createSession', room).then((response) => {
      console.log('Room created:', response);
      const sessionId = response.sessionId;
      console.log(sessionId);
      this.router.navigate(['/game', sessionId]);
    }).catch((error) => console.error('Error creating room:', error));
  }

  joinRoom() {
    const roomId: HTMLInputElement | null = document.querySelector('input[name="roomId"]');
    if (!roomId?.value) {
      console.error('No room ID provided');
      return;
    }

    console.log('Joining room:', roomId.value);
    this.router.navigate(['/game', roomId.value]);
  }

  testValid(roomId: HTMLInputElement): void {
    const button = document.querySelector('button[name="joinRoom"]');
    if (roomId?.value.length >= 4) {
      button?.removeAttribute('disabled');
    } else {
      button?.setAttribute('disabled', 'disabled');
    }
  }
}
