import { Component } from '@angular/core';
import { LoginComponent } from "../login/login.component";
import { GameRoomManagerComponent } from "../game-room-manager/game-room-manager.component";
import { CommonModule } from '@angular/common';
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'app-home',
    standalone: true,
    templateUrl: './home.component.html',
    styleUrl: './home.component.scss',
    imports: [LoginComponent, GameRoomManagerComponent, CommonModule]
})
export class HomeComponent {
  loggedIn = false;

  constructor(private authService: AuthService) {
    this.authService.loggedIn.subscribe(loggedIn => {
      this.loggedIn = loggedIn;
    });
  }
}
