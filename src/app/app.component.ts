import { Component } from '@angular/core';
import { RouterModule, RouterOutlet } from '@angular/router';
import { LoginComponent } from "./login/login.component";
import { CommonModule } from '@angular/common';
import { TopBarComponent } from "./top-bar/top-bar.component";
import { GameRoomManagerComponent } from "./game-room-manager/game-room-manager.component";

@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss',
    imports: [
      RouterModule,
      RouterOutlet,
      LoginComponent,
      CommonModule,
      TopBarComponent,
      GameRoomManagerComponent]
})
export class AppComponent {
  title = 'coup-game';
}
