import { Routes } from '@angular/router';
import { GameRoomComponent } from './game-room/game-room.component';
import { HomeComponent } from './home/home.component';

export const routes: Routes = [
    { path: '', component: HomeComponent, title: 'Home' },
    { path: 'game/:id', component: GameRoomComponent, title: 'Game Room' }
];
