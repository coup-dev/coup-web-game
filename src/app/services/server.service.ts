import { Injectable } from "@angular/core";

@Injectable({ 
    providedIn: 'root' 
})
export class ServerService {
    constructor() {}

    async postToServer(url: String, data: Record<string, any>) {
        let encodedData = '';
        for (const key in data) {
            encodedData += encodeURIComponent(key) + '=' + encodeURIComponent(data[key]) + '&';
        }
        const bodyData: BodyInit = encodedData.slice(0, -1);
        return await fetch('http://localhost:6554/' + url, {
            method: 'POST',
            body: bodyData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            }
        }).then((response) => {
            return response.json();
        });
    }
}