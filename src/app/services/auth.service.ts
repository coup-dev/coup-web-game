import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({ 
    providedIn: 'root' 
})
export class AuthService {
    loggedIn = new BehaviorSubject<boolean>(false);
    id: number | null = null;
    username: string | null = null;
    constructor() { }
    login(id: number, username: string) {
        this.id = id;
        this.username = username;
        this.loggedIn.next(true);
    }
    logout() {
        this.loggedIn.next(false);
    }
}