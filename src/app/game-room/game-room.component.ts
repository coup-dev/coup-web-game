import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game-room',
  standalone: true,
  imports: [],
  templateUrl: './game-room.component.html',
  styleUrl: './game-room.component.scss'
})
export class GameRoomComponent implements OnInit {
  constructor() {
    console.log('GameRoomComponent created')
  }

  ngOnInit(): void {
    console.log('GameRoomComponent initialized')
  }
}
