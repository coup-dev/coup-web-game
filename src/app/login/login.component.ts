import { Component } from '@angular/core';
import { ServerService } from '../services/server.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  // Get the password and username from the HTML form
  // and send it to the server


  constructor(private serverService: ServerService,
              private authService: AuthService
  ) {}

  login(username: string, password: string) {
    this.serverService.postToServer('login', 
      {username: username, password: password}
    ).then((response) => {
      console.log('Logged in');
      console.log(response);
      this.authService.login(response?.id, username)
    });
  }

  register(username: string, password: string) {
    this.serverService.postToServer('register', 
      {username: username, password: password}
    ).then((response) => {
      console.log('Registered');
      console.log(response);
    });
  }
}
